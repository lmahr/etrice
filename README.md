# eTrice

The project info and wiki is located here: [eTrice wiki](https://gitlab.eclipse.org/groups/eclipse/etrice/-/wikis/home)

## Development

### Eclipse

You can automate the workspace setup with the Oomph setup file [team/ETrice.setup](team/ETrice.setup), see [instructions](https://gitlab.eclipse.org/groups/eclipse/etrice/-/wikis/Development/GettingStartedWithEtrice).

Alternatively install from the Eclipse update site:

* Xtext
* Graphiti
* Ecore
* CDT

and from the Eclipse Orbit update site `https://download.eclipse.org/tools/orbit/downloads/drops/R20230531010532`:

* org.apache.commons.io
* com.atlassian.commonmark(-*)

If you want to reproduce the CI build, see [releng/org.eclipse.etrice.parent/pom.xml](releng/org.eclipse.etrice.parent/pom.xml).

### Build

The Tycho/Maven for the Eclipse P2 plugins and plugin tests
```
mvn verify
```

The Gradle build for the headless generators and generator tests
```
.\gradlew build
```

In order to build eTrice C/C++ projects and tests with Gradle, a GCC toolchain must be available on the `PATH`.
For Windows the latest release of MinGW-w64 with UCRT runtime and POSIX threads is recommended, which can be downloaded from [WinLibs](https://winlibs.com/).

## Publish

Release builds (commit has version tag) are published to Maven [etrice-releases](https://repo.eclipse.org/content/repositories/etrice-releases) repository and P2 site [download.eclipse.org/etrice](https://download.eclipse.org/etrice). 

Master builds without tag are published to Maven [etrice-snapshots](https://repo.eclipse.org/content/repositories/etrice-snapshots) repository with version `<last version>-SNAPSHOT` and the P2 site [download.eclipse.org/etrice/nightly](https://download.eclipse.org/etrice/nightly). 

All other builds are published to Maven [etrice-snapshots](https://repo.eclipse.org/content/repositories/etrice-snapshots) repository. The version is set to `<last version>-<branch>-SNAPSHOT`, where special chars are replaced with `-`. 
In any case, you can see the current version in the console output via `.\gradlew`.

