Ports
=====

## Description

Ports are the only interfaces of actors. A port has always a protocol assigned. Service Access Points (SAP) and Service Provision Points (SPP) are specialized ports that are used to define layering.

## Motivation

-   Ports decouple interface definition (protocols) from interface usage

-   Ports decouple the logical interface from the transport

## Notation

### Class Ports

Ports that define an external interface of the actor class, are defined in the Interface. Ports that define an internal interface are defined in the Structure (e.g. internal ports).

-   *External end ports* are defined in the Interface and in the Structure

-   *Internal end ports* are only defined in the Structure

-   *Relay ports* are only defined in the Interface

-   *End ports* are always connected to the internal behavior of the ActorClass

-   *Replicated ports* can be defined with a fixed replication factor, e.g.  
	`Port port18 [5]: ProtocolClass1`  
    or a variable replication factor, e.g.  
	`Port port18[*]: ProtocolClass1`

-   The graphical symbols of Interface ports are drawn on the border of the actor class. The graphical symbols of Structure ports are drawn inside the border of an actor class.

The table below shows all kinds of class ports with textual and graphical notation:

<table class="table">
<thead>
<tr>
<th><strong>Element</strong></th>
<th><strong>Graphical Notation</strong></th>
<th><strong>Textual Notation</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>Class End Port </td>
<td>

![](../images/040-ClassEndPort.png)

</td>
<td>

*External Class End Port:*

```room
ActorClass ActorClass6 {
	Interface {
		Port port12: ProtocolClass1
	}
	Structure {
		external Port port12
	}
}
```
*Internal Class End Port:*

```room
ActorClass ActorClass6 {
	Interface { }
	Structure {
		Port port20
	}
}
```

</td>
</tr>
<tr>
<td>Conjugated Class End Port </td>
<td>

![](../images/040-ConjugatedClassEndPort.png)

</td>
<td>

*External Conjugated Class End Port:*

```room
ActorClass ActorClass6 {
	Interface {
		conjugated Port port13: ProtocolClass1
	}
	Structure {
		external Port port13
	}
}
```
*Internal Conjugated Class End Port:*

```room
ActorClass ActorClass6 {
	Interface {}
	Structure {
		conjugated Port port21: ProtocolClass1
	}
}
```

</td>
</tr>
<tr>
<td>Class Relay Port </td>
<td>

![](../images/040-ClassRelayPort.png)

</td>
<td>

```room
ActorClass ActorClass6 {
	Interface {
		Port port10: ProtocolClass1
	}
	Structure { }
}
```

</td>
</tr>
<tr>
<td>Conjugated Class Relay Port </td>
<td>

![](../images/040-ConjugatedClassRelayPort.png)

</td>
<td>

```room
ActorClass ActorClass6 {
	Interface {
		conjugated Port port10: ProtocolClass1
	}
	Structure {}
}
```

</td>
</tr>
<tr>
<td>Replicated Class End Port </td>
<td>

![](../images/040-ReplicatedClassEndPort.png)

</td>
<td>

*External Replicated Class End Port:*

```room
ActorClass ActorClass6 {
	Interface {
		Port port16[3]: ProtocolClass1
	}
	Structure {
		external Port port16
	}
}
```
*Internal Replicated Class End Port:*

```room
ActorClass ActorClass6 {
	Interface {}
	Structure {
		Port port16[3]: ProtocolClass1
	}
}
```

</td>
</tr>
<tr>
<td>Conjugated Replicated Class End Port </td>
<td>

![](../images/040-ConjugatedReplicatedClassEndPort.png)

</td>
<td>

*External Conjugated Replicated Class End Port:*

```room
ActorClass ActorClass6 {
	Interface {
		conjugated Port port17[3]: ProtocolClass1
	}
	Structure {
		external Port port17
	}
}
```
*Internal Conjugated Replicated Class End Port:*

```room
ActorClass ActorClass6 {
	Interface { }
	Structure {
		conjugated Port port23[3]: ProtocolClass1
	}
}
```

</td>
</tr>
<tr>
<td>Replicated Class Relay Port </td>
<td>

![](../images/040-ReplicatedClassRelayPort.png)

</td>
<td>

```room
ActorClass ActorClass6 {
	Interface {
		Port port18[3]: ProtocolClass1
	}
	Structure { }
}
```

</td>
</tr>
<tr>
<td>Conjugated Replicated Class Relay Port </td>
<td>

![](../images/040-ConjugatedReplicatedClassRelayPort.png)

</td>
<td>

```room
ActorClass ActorClass6 {
	Interface {
		conjugated Port port19[3]: ProtocolClass1
	}
	Structure { }
}
```

</td>
</tr>
</tbody>
</table>


### Reference Ports

These symbols can only appear on the border of an actor class reference. Since the type of port is defined in the respective actor class, no textual notation for the Reference Ports exists.

The table below shows all kinds of reference ports with textual and graphical notation:

|        **Element**        |                   **Graphical Notation**                   | **Textual Notation** |
|:-------------------------:|:----------------------------------------------------------:|:--------------------:|
|       Reference Port      |           ![image](../images/040-ReferencePort.png)           |      *implicit*      |
| Conjugated Reference Port |      ![image](../images/040-ConjugatedReferencePort.png)      |      *implicit*      |
| Replicated Reference Port |      ![image](../images/040-ReplicatedReferencePort.png)      |      *implicit*      |
|   Conjugated Replicated Reference Port      | ![image](../images/040-ConjugatedReplicatedReferencePort.png) |      *implicit*      |
