ROOM Concepts
=============

This chapter gives an overview over the ROOM language elements and their textual and graphical notation. The formal ROOM grammar based on Xtext (EBNF) you can find in the eTrice repository:
<http://git.eclipse.org/c/etrice/org.eclipse.etrice.git/plain/plugins/org.eclipse.etrice.core.room/src/org/eclipse/etrice/core/Room.xtext>

**Abbreviations** 
* SPP: Service Provising Point
* SAP: Service Access Point
* FSM: finite-state machine
