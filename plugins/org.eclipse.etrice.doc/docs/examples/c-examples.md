The C examples project
======================

This example project `org.eclipse.etrice.examples.c` illustrates a simplified traffic light control. It actually consists of four different steps of increasing complexity that can be generated and built one by one (but not in parallel).

Most of the classes are contained in a common ROOM model `model/TrafficLight/TrafficLight.room` that also shows how ROOM classes can form a library and be used as building blocks for several systems. Also the physical model is shared by all steps of this example.

Contained in the example you will find a little Java GUI that some of the examples use to simulate a real traffic light. The GUI is contacted using TCP/IP ports 4441 and subsequent. The protocol used on the TCP/IP socket connection is a simple text protocol with commands for the lights. Three of the example steps make use of this simulator GUI. They also contain a launch configuration for it.

:::note
Before compiling and linking a new step of the example the project should be cleaned to avoid duplicate symbols due to artifacts from the previous build.
:::

## Traffic light - step 1

The main ROOM model and the mapping model of this example is contained in `org.eclipse.etrice.examples.c/model/TrafficLight_step1`. It also contains launch configurations for the code generation and for running the application.

### Purpose

This example illustrates how a single actor can be tested against a test harness.

### Details

A `PedestrianTrafficLightController` is an actor that controls a traffic light consisting of a traffic light with red, yellow and green for the cars, a second traffic light with just red and green and a request button for the pedestrians.

The controller has two ports, `fct` and `tlInterface`. The first one uses a protocol `PTrafficLightController` that offers the high level commands `standby`, `start` and `stop`. The second one uses a conjugated protocol `PTrafficLightInterface` which controls each of the lights separately.

The test harness `TrafficLightControllerTestHarness` is connected to both ports and drives the test in several steps.

To run the test first generate the code using `gen_TrafficLight_step1.launch`, then compile and link it (using the CDTbuild). Since this step doesn’t use a GUI you can just start the application using `run_TrafficLight_step1.launch`.

### Noteworthy

-   usage of a ROOM model as library for several systems

-   creation of a test harness for an actor

## Traffic light - step 2

The main ROOM model and the mapping model of this example is contained in `org.eclipse.etrice.examples.c/model/TrafficLight_step2`. It also contains launch configurations for the code generation, for launching the simulator GUI and for running the application.

### Purpose

This example shows the control of a single block of traffic lights containing a car light and a light for the pedestrians. It illustrates how a stack of abstraction levels is used to conveniently divide the problem into relatively simple parts.

### Details

The 4 levels of this application starts with a `TrafficLightHeadquarters` on the top. It controls the global operating mode of the traffic light block. The test harness `TrafficLightControllerTestHarness` is connected to both ports and drives the test in several steps.

On the next level we have a `PedestrianTrafficLightController` which offers some very general commands like `start`, `stop` and `standby`.

On the second level we find an actor `PTrafficLightInterface` that offers basic traffic light control in the form of switching the state of each single light separately.

On the base level we have an actor handling a TCP/IP socket connection with a general text protocol.

To run the test first generate the code using `gen_TrafficLight_step2.launch`, then compile and link it (using the CDTbuild). Since this step uses a GUI with a single block of lights you’ll have to launch the simulator using `run_simulator_1_block.launch`. Then you can just start the application using `run_TrafficLight_step2.launch`.

After going through a short initialization and standby phase the traffic light goes into a mode of permanent green for cars. After pressing the ’request’ button in the simulator GUI the light switches to red for cars and green for pedestrians and back to permanent green for cars.

### Noteworthy

-   use abstraction levels to break a problem into smaller pieces.

## Traffic light - step 3

The main ROOM model and the mapping model of this example is contained in `org.eclipse.etrice.examples.c/model/TrafficLight_step3`. It also contains launch configurations for the code generation, for launching the simulator GUI and for running the application.

### Purpose

This example shows the control of two blocks of traffic lights each containing a car light and a light for the pedestrians. It uses a replicated stack of actors as introduced in step 2.

### Details

The head quarters actor of this example controls two blocks of traffic lights. It already has a replicated port which is now connected to a doubled `PedestrianTrafficLightController`. Of course the replication then has also to be applied to the other levels of the stack introduced in step 2.

To run the test first generate the code using `gen_TrafficLight_step3.launch`, then compile and link it (using the CDTbuild). Since this step uses a GUI with a two blocks of lights you’ll have to launch the simulator using `run_simulator_2_blocks.launch`. Then you can just start the application using `run_TrafficLight_step3.launch`.

### Noteworthy

-   use replication for multiple instances of the same.

## Traffic light - step 4

The main ROOM model and the mapping model of this example is contained in `org.eclipse.etrice.examples.c/model/TrafficLight_step4`. It also contains launch configurations for the code generation, for launching the simulator GUI and for running the application.

### Purpose

This example shows the control of two blocks of traffic lights each containing a car light and a light for the pedestrians. It uses a container actor for the three lower levels of the stack introduced in step 2. Then replication has to applied only to the container actor which simplifies the task.

### Details

The head quarters actor of this example controls two blocks of traffic lights. It already has a replicated port which is now connected to a doubled `TrafficLightControllerWithInfrastructure`. The latter one

### Noteworthy

-   use hierarchy to group levels functionality to reusable blocks.

## Features

This project illustrates the various ways how replication can be exploited in ROOM. Replication can be chosen for ports and for actor references. The example enumerates a number of typical combinations of the two possibilities.

### Purpose

Five different ways to combine replicated ports and replicated actor references are shown in this example.

Each of these combination consists of one ore more receivers and one or more senders.

All five combinations are contained in the actor class `ExampleMultiplicity`.

-   `sender1` and `receiver1` - a single sender and a single receiver, both with plain ports, are connected.

-   `multiSender2` with a replication factor of 10 is connected to `multiReceiver2` also with a replication factor of 10 - this is equivalent to 10 single senders each of which is connected to one single out of 10 receivers

-   `multiSender3` with a replication factor of 2 is connected to `receiverMultiPort3` which has a port with replication factor 2

-   `multiSender4` with a replication factor of 10 is connected to `multiReceiverMultiPort4` with a replicated port with cardinality 2 and replicated 5 times - this is equivalent to 5 receivers where each is connected to two senders

-   `multiSender5` is connected to

    -   `receiver5` which is a simple receiver not using any replication

    -   `multiReceiver5` which is a replicated receiver

    -   `` which is not replicated but uses a replicated portreceiverMultiPort5

    -   `multiReceiverMultiPort5` which is replicated and has replicated ports

    So `multiSender5` is connected

    -   once to `receiver5`

    -   three times to `multiReceiver5`

    -   two times to `receiverMultiPort5` and

    -   four times to `multiReceiverMultiPort5`

    which totals to 10 (implicit) connections and matches the replication factor of 10 of `multiSender5`.

### Details

The head quarters actor of this example controls two blocks of traffic lights. It already has a replicated port which is now connected to a doubled `TrafficLightControllerWithInfrastructure`. The latter one

### Noteworthy

-   use hierarchy to group levels of functionality to reusable blocks.
