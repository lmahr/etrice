Prism.languages.etphys = {
	'comment': {
		pattern: /(?:\/\*[\s\S]*?\*\/)|(?:\/\/.*)/,
		greedy: true
	},
	'string': {
		pattern: /"(?:\\(?:\r\n|[\s\S])|[^"\\\r\n])*"/,
		greedy: true
	},
	'punctuation': /[{}:]/,
    'operator': /=|->/,
	'number': /\b\d+\b/,
	'keyword': /\b(?:import|from|model|PhysicalModel|RuntimeClass|DefaultThread|PhysicalSystem|NodeRef|NodeClass|Thread)\b/
};
