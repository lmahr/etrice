/*******************************************************************************
 * Copyright (c) 2012 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Juergen Haug
 * 
 *******************************************************************************/

package org.eclipse.etrice.ui.structure.support;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.etrice.core.room.ActorContainerRef;
import org.eclipse.etrice.core.room.Binding;
import org.eclipse.etrice.core.room.InterfaceItem;
import org.eclipse.etrice.core.room.LayerConnection;
import org.eclipse.etrice.core.room.StructureClass;
import org.eclipse.etrice.core.room.util.RoomSwitch;
import org.eclipse.graphiti.mm.PropertyContainer;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.ILinkService;
import org.eclipse.graphiti.services.IPeService;

public class DiagramUtil {
	
	public static enum Border {
		NONE,
		TOP,
		BOTTOM,
		LEFT,
		RIGHT
	}

	public static boolean isSupported(Object obj){
		 return  obj instanceof StructureClass
				 || obj instanceof ActorContainerRef
				 || obj instanceof InterfaceItem
				 || obj instanceof Binding 
				 || obj instanceof LayerConnection;
	}
	
	public static boolean isConnection(EObject bo){
		return bo instanceof Binding
				|| bo instanceof LayerConnection;
	}
	
	public static String getResourcePath(EObject bo){
		assert(isSupported(bo)) : "unexpected type";
		return bo.eResource().getURI().toString()+bo.eResource().getURIFragment(bo);
	}
	
	public static ContainerShape findScShape(Diagram diagram){
		ILinkService linkService = Graphiti.getLinkService();
		for(Shape child : diagram.getChildren()){
			Object bo = linkService.getBusinessObjectForLinkedPictogramElement(child);
			if(bo instanceof StructureClass)
				return (ContainerShape) child;
		}
		
		return null;
	}
	
	public static PosAndSize getPosAndSize(GraphicsAlgorithm ga){
		return new PosAndSize(ga.getX(), ga.getY(), ga.getWidth(), ga.getHeight());
	}
	
	public static PropertyContainer findProp(List<? extends PropertyContainer> items, String key, String value){
		IPeService peService = Graphiti.getPeService();
		for(PropertyContainer p : items)
			if(value.equals(peService.getPropertyValue(p, key)))
				return p;
		
		return null;
	}

	/**
	 * Given an element and its container, calculates the nearest border relative to the element's boundaries.
	 */
	public static Border getNearestBorder(PosAndSize element, PosAndSize container) {
		int left = Math.abs(element.getX());
		int right = Math.abs(container.getW() - element.getX());
		int top = Math.abs(element.getY());
		int bottom = Math.abs(container.getH() - element.getY());
		int min = left;
		Border border = Border.LEFT;
		if (right < min) {
			border = Border.RIGHT;
			min = right;
		}
		if (top < min) {
			border = Border.TOP;
			min = top;
		}
		if (bottom < min) {
			border = Border.BOTTOM;
		}
		return border;
	}

	/**
	 * Given an element and its container, calculates the nearest border relative to the element's boundaries.
	 * Takes the position and size values from the GraphicsAlgorithm objects.
	 */
	public static Border getNearestBorder(GraphicsAlgorithm element, GraphicsAlgorithm container) {
		PosAndSize posContainer = getPosAndSize(container);
		PosAndSize pos = getPosAndSize(element);
		return getNearestBorder(pos, posContainer);
	}

	private static class VisibleExtentSwitch extends RoomSwitch<PosAndSize> {
		private PictogramElement pe; 
		public VisibleExtentSwitch(PictogramElement pe) {
			this.pe = pe;
		}

		@Override
		public PosAndSize caseStructureClass(StructureClass object) {
			if (pe.getGraphicsAlgorithm() == null)
				return null;
			if (pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().size() < 2) {
				return null;
			}
			
			GraphicsAlgorithm visibleGa = pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0);
			return getPosAndSize(visibleGa);
		}

		@Override
		public PosAndSize caseInterfaceItem(InterfaceItem object) {
			if (pe.getGraphicsAlgorithm() == null)
				return null;
			if (pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().size() < 1) {
				return null;
			}
			GraphicsAlgorithm invisible = pe.getGraphicsAlgorithm();
			GraphicsAlgorithm visible = pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0);
			return new PosAndSize(
					visible.getX() + invisible.getX(),
					visible.getY() + invisible.getY(),
					visible.getWidth(),
					visible.getHeight()
			);
		}

		@Override
		public PosAndSize caseActorContainerRef(ActorContainerRef object) {
			if (pe.getGraphicsAlgorithm() == null)
				return null;
			if (pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().size() < 2) {
				return null;
			}
			
			IPeService peService = Graphiti.getPeService();
			GraphicsAlgorithm invisible = pe.getGraphicsAlgorithm();
			GraphicsAlgorithm main = null;
			GraphicsAlgorithm repl = null;
			boolean isReplVisible = false;
			for (GraphicsAlgorithm ga : invisible.getGraphicsAlgorithmChildren()) {
				String value = peService.getPropertyValue(ga, ActorContainerRefSupport.GRAPHIC_ITEM_KEY);
				if (ActorContainerRefSupport.MAIN_BORDER.equals(value)) {
					main = ga;
				} else if (ActorContainerRefSupport.REPL_BORDER.equals(value)) {
					repl = ga;
					isReplVisible = ga.getLineVisible();
				}
			}

			if (main == null || repl == null)
				return null;

			if (isReplVisible) {
				int x = Math.min(main.getX(), repl.getX());
				int y = Math.min(main.getY(), repl.getY());
				int wTotalMain = main.getX()+main.getWidth();
				int wTotalRepl = repl.getX()+repl.getWidth();
				int hTotalMain = main.getY()+main.getHeight();
				int hTotalRepl = repl.getY()+repl.getHeight();
				return new PosAndSize(
						x+invisible.getX(),
						y+invisible.getY(),
						wTotalMain > wTotalRepl ? wTotalMain - x : wTotalRepl - x,
						hTotalMain > hTotalRepl ? hTotalMain - y : hTotalRepl - y);
			} else {
				return new PosAndSize(
						main.getX()+invisible.getX(),
						main.getY()+invisible.getY(),
						main.getWidth(), main.getHeight());
			}
		}

		@Override
		public PosAndSize defaultCase(EObject object) {
			return getPosAndSize(pe.getGraphicsAlgorithm());
		}
	}

	/**
	 * Gets the position and size of the visible bounding box of the given PictogramElement. The
	 * bounds will be calculated based on the GraphicsAlgorithms of the PictogramElement and the
	 * type of business object it is associated with.
	 *
	 * @return the visible bounds of the PictogramElement, or null if the PictogramElement was malformed 
	 * or the corresponding business object was invalid 
	 */
	public static PosAndSize getVisibleExtent(PictogramElement pe) {
		Object bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
		if (!EObject.class.isInstance(bo))
			return null;

		return new VisibleExtentSwitch(pe).doSwitch((EObject)bo);
	}
}
