/*******************************************************************************
 * Copyright (c) 2022 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * CONTRIBUTORS:
 * 		Eyrak Paen-Rochlitz (initial contribution)
 *
 *******************************************************************************/

#include "TestEtUnitNegative.h"

#include "etUnit/etUnit.h"

void TestEtUnitNegative_ExpectEqualStr(etInt16 id){
	char str1[] = "abc";
	char str2[] = "123";
	EXPECT_EQUAL_STR(id, "EXPECT_EQUAL_STR", str1, str2);
}

void TestEtUnitNegative_ExpectEqualStr_ExpectedIsNull(etInt16 id){
	char str[] = "abc";
	EXPECT_EQUAL_STR(id, "EXPECT_EQUAL_STR", NULL, str);
}

void TestEtUnitNegative_ExpectEqualStr_ActualIsNull(etInt16 id){
	char str[] = "abc";
	EXPECT_EQUAL_STR(id, "EXPECT_EQUAL_STR", str, NULL);
}

void TestEtUnitNegative_runSuite(void){
	etUnit_openTestSuite("org.eclipse.etrice.runtime.c.tests.TestEtUnitNegative");
	ADD_TESTCASE(TestEtUnitNegative_ExpectEqualStr);
	ADD_TESTCASE(TestEtUnitNegative_ExpectEqualStr_ExpectedIsNull);
	ADD_TESTCASE(TestEtUnitNegative_ExpectEqualStr_ActualIsNull);
	etUnit_closeTestSuite();
}
