// See https://wiki.eclipse.org/Jenkins for information about the Eclipse Jenkins continuous integration server
pipeline {
	agent {
		kubernetes {
			label 'migration'
		}
	}
	tools {
		maven 'apache-maven-latest'
		jdk 'adoptopenjdk-hotspot-jdk11-latest'
	}
	options {
		timeout(time: 60, unit: 'MINUTES')
	}
	// We change the Gradle home directory because we don't have access rights to the default location ${USER_HOME}/.gradle.
	// Usually this is possible by setting the GRADLE_USER_HOME environment variable. However, this doesn't work here for some reason.
	// environment {
	// 	GRADLE_USER_HOME = './gradle_home'
	// }
	stages {
		stage('Build') {
			steps {
				wrap([$class: 'Xvnc', takeScreenshot: false, useXauthority: true]) {
					// exclude build of vscode-extension from ci build because I don't know how to get/execute nodejs on jenkins.
					sh './gradlew clean build -x :vscode-extension:build -Pmaven -Psign -Dgradle.user.home=./gradle_home'
				}
				recordIssues(/* unstableTotalAll: ?, */ tool: gcc(id: 'runtime', name: 'runtime', pattern: 'runtime/*/build/tmp/*/output.txt'), trendChartType: 'TOOLS_ONLY')
				recordIssues(tool: gcc(id: 'tests', name: 'tests', pattern: 'tests/*/build/tmp/*/output.txt'), trendChartType: 'TOOLS_ONLY')
			}
		}
		stage('Deploy Maven') {
			steps {
				sh './gradlew publishEtricePublicationToEclipseRepository -Dgradle.user.home=./gradle_home -PadditionalPropertiesFile=/home/jenkins/.gradle/gradle.properties'
			}
		}
		stage('Deploy P2') {
			when {
				branch 'master'
			}
			steps {
				sshagent(['projects-storage.eclipse.org-bot-ssh']) {
					sh './gradlew promote -Dgradle.user.home=./gradle_home -PadditionalPropertiesFile=/home/jenkins/.gradle/gradle.properties'
				}
			}
		}
	}
	post {
		always {
			archiveArtifacts '*/*/build/**'
			junit 'tests/*/target/surefire-reports/**/*.xml, tests/*/build/log/**/*.xml'
		}
		// send a mail on unsuccessful and fixed builds
		unsuccessful { // means unstable || failure || aborted
			emailext subject: 'Build $BUILD_STATUS $PROJECT_NAME #$BUILD_NUMBER!', 
				body: '''Check console output at $BUILD_URL to view the results.''',
				recipientProviders: [culprits(), requestor()]
		}
		fixed { // back to normal
			emailext subject: 'Build $BUILD_STATUS $PROJECT_NAME #$BUILD_NUMBER!', 
				body: '''Check console output at $BUILD_URL to view the results.''',
				recipientProviders: [culprits(), requestor()]
		}
	}
}